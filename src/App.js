import React, {useState} from 'react';
import './App.css';
import Menu from "./components/Menu/Menu";
import meatImage from './assets/1-10782_meat-clipart-beef-clipart-png-download.jpeg';
import saladImage from  './assets/pngtree-food-food-lettuce-cute-png-image_4750070.jpg';
import cheeseImage from './assets/41992160-happy-cartoon-cheese-character.jpg';
import beaconImage from './assets/bacon-strip-running-thumbs-up-vector-illustration-cartoon-68748196.jpg';
import Burger from "./components/Burger/Burger";
import Counter from "./components/Counter/Counter";

const INGREDIENTS = [
  {name: 'Meat', price: 50, image: meatImage},
  {name: 'Salad', price: 5, image: saladImage},
  {name: 'Cheese', price: 20, image: cheeseImage},
  {name: 'Bacon', price: 30, image: beaconImage},
];

const App = () => {
  const [ingredients, setIngredients] = useState([
    {name: 'Meat', count: 0},
    {name: 'Salad', count: 0},
    {name: 'Cheese', count: 0},
    {name: 'Bacon', count: 0},
  ]);


  const addIngred = ind => {
    const copyIngredients = [...ingredients];
    let ingrCount = {...copyIngredients[ind]};
    ingrCount.count++;
    copyIngredients[ind] = ingrCount;
    setIngredients(copyIngredients);
  }

  const removeIngred = ind => {
    const copyIngredients = [...ingredients];
    let ingrCount = {...copyIngredients[ind]};
    if (ingrCount.count > 0) {
      ingrCount.count--;
      copyIngredients[ind] = ingrCount;
      setIngredients(copyIngredients);
    }
  };

  const menuString = INGREDIENTS.map((ingred, index) => {
    return <Menu
        key={index}
        ingredient={ingredients[index]}
        name={ingred.name}
        image={ingred.image}
        add={() => addIngred(index)}
        remove={() => removeIngred(index)}
    />
  })

  return (

    <div className="App">
      <h1>Create your own Burger!</h1>
      <div className="container">
      <div>
        {menuString}
      </div>
      <Burger
          ingredients = {ingredients}
      />
      </div>
      <Counter
          INGREDIENTS = {INGREDIENTS}
          ingredients = {ingredients}
      />
    </div>
  );
}


export default App;
