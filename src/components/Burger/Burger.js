import React from 'react';
import './Burger.css'
import BreadTop from "../BreadTop/BreadTop";
import BreadBottom from "../BreadBottom/BreadBottom";
import Bacon from "../Bacon/Bacon";
import Cheese from "../Cheese/Cheese";
import Meat from "../Meat/Meat";
import Salad from "../Salad/Salad";
import { nanoid } from 'nanoid';


const Burger = props => {
    const arr = [];
    props.ingredients.forEach(ing => {
        if (ing.name === 'Bacon') {
            for (let i = 0; i < ing.count; i++) {
                const key = nanoid()
                arr.push(<Bacon key={key}/>)
            }
        } else if (ing.name === 'Cheese') {
            for (let i = 0; i < ing.count; i++) {
                const key = nanoid()
                arr.push(<Cheese key={key}/>)
            }
        } else if (ing.name === 'Meat') {
            for (let i = 0; i < ing.count; i++) {
                const key = nanoid()
                arr.push(<Meat key={key}/>)
            }
        } else if (ing.name === 'Salad') {
            for (let i = 0; i < ing.count; i++) {
                const key = nanoid()
                arr.push(<Salad key={key}/>)
            }
        }
    })

    return (
        <div className="Burger">
            <BreadTop/>
            {arr}
            <BreadBottom/>
        </div>
    );
};

export default Burger;