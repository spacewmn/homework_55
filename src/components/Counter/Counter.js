import React from 'react';

const Counter = props => {

    const ingrGlob = props.INGREDIENTS;

    let price = [20];
    let sum = 0;

    props.ingredients.forEach((ing, index) => {
        if (ing.name === ingrGlob[index].name) {
            // let beginCount
            // console.log(ingrGlob[0].name)
            price.push(ing.count * ingrGlob[index].price);

        }
    });

    for (let i=0; i<price.length; i++){
        sum = sum + parseInt(price[i]);
    }

    return (
        <h4>
            Price: {sum}
        </h4>
    );
};

export default Counter;