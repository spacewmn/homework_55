import React from 'react';
import './Menu.css'

const Menu = props => {
    // const url = `url(${props.image})`

    return (
        <div className="Menu">
            <img onClick={props.add} src={props.image} alt="ingr"/>
            <p>{props.name}</p>
            <p>x {props.ingredient.count}</p>
            <button type="button" onClick={props.remove}>Delete</button>
        </div>
    );
};

export default Menu;